import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy import nan
import collections

def plot_bar(match_per_season):
    # this is for plotting purpose
    seasons = list(match_per_season.keys())
    matches = list(match_per_season.values())
    plt.bar(seasons, matches)
    plt.xlabel('Season', fontsize=15)
    plt.ylabel('No of Matches', fontsize=15)
    plt.xticks(seasons, fontsize=10, rotation=30)
    plt.title('Match per season 2008-2017')
    plt.show()


def stacked_bar_graph(winning_dict):
    teams = list(winning_dict.keys())
    seasons = list(winning_dict[teams[0]].keys())
    N = 14
    ind = np.arange(N)
    scores = {}
    for season in seasons:
        wins = []
        for team in teams:
            wins.append(winning_dict[team][season])
        scores[season] = wins
    p1 = plt.bar(ind, scores[seasons[0]], width=0.35)
    for i in range(1, len(seasons)):
        p2 = plt.bar(ind, scores[seasons[i]],
                     bottom=scores[seasons[i-1]], width=0.35)
    plt.ylabel('Wins')
    plt.title('Stacked bar graph of Wins by All teams')
    plt.xticks(ind, teams, fontsize=5, rotation=40)
    plt.yticks(np.arange(0, 30, 5))
    plt.show()

def extra_runs_plot_bar(runs):
    # this is for plotting purpose
    teams = list(runs.keys())
    extra_runs = list(runs.values())
    plt.bar(teams, extra_runs)
    plt.xlabel('Teams', fontsize=15)
    plt.ylabel('Extra Runs', fontsize=15)
    plt.xticks(teams, fontsize=10, rotation=30)
    plt.title('Extra run in 2016')
    plt.show()

def economy_rate_plot_bar(economy_rate):
    # this is for plotting purpose
    bowlers = list(economy_rate.keys())[:10]
    rate = list(economy_rate.values())[:10]
    plt.bar(bowlers, rate)
    plt.xlabel('Bowlers', fontsize=15)
    plt.ylabel('Economy Rate', fontsize=15)
    plt.xticks(bowlers, fontsize=10, rotation=30)
    plt.title('Economy Rate of Top 10 Bowlers in 2015')
    plt.show()


data = pd.read_csv("matches.csv")
match_per_season = dict(data['season'].value_counts())
plot_bar(match_per_season)

seasons = list(set(data['season']))
seasons.sort()
teams = list(set(data['winner']))
winning_dict = {}
for team in teams:
    if team is not nan:
        df = pd.DataFrame(data)
        team_df = df[df.winner == team]
        season_dict = {}
        for season in seasons:
            wins = team_df[team_df.season == season].count()
            season_dict[season] = wins['id']
        winning_dict[team] = season_dict

stacked_bar_graph(winning_dict)


dataframe = pd.DataFrame(data)
match_ids = list(dataframe[dataframe.season == 2016].id)
delivery_data = pd.read_csv('deliveries.csv')
df = pd.DataFrame(delivery_data)
runs = {}

for team in teams:
    if team is nan:
        continue
    runs[team] = 0

for match_id in match_ids:
    match_df = df[df.match_id == match_id]
    for i in range(1, 3):
        inning_df = match_df[match_df.inning == i]
        runs[inning_df.batting_team.iloc[0]
             ] = runs[inning_df.batting_team.iloc[0]]+inning_df.extra_runs.sum()

extra_runs_plot_bar(runs)

match_ids = list(dataframe[dataframe.season == 2015].id)
runs = {}
bowls = {}
bowlers = delivery_data['bowler'].unique().tolist()
for bowler in bowlers:
    runs[bowler] = 0
    bowls[bowler] = 0

for match_id in match_ids:
    match_df = df[df.match_id == match_id]
    match_bowlers = match_df.bowler.unique().tolist()
    for bowler in match_bowlers:
        bowler_df = match_df[match_df.bowler == bowler]
        bowls[bowler] = bowls[bowler] + bowler_df.count().iloc[1]
        runs[bowler] = runs[bowler] + bowler_df.total_runs.sum()

economy_rate = {}
for bowler in bowlers:
    if bowls[bowler] == 0:
        continue
    overs = bowls[bowler]/6
    economy_rate[bowler] = runs[bowler]/overs
economy_rate = sorted(economy_rate.items(), key = 
             lambda kv:(kv[1], kv[0]))
economy_rate = collections.OrderedDict(economy_rate)
economy_rate_plot_bar(economy_rate)